"""---------------------------------------------------------------------------.

    Workfile: countwords.py

    Project: Sandbox test code

       Copyright (c) 2021
        Fresh Consulting
      All Rights Reserved

    This software may not be reproduced, in part or in its entirety,
    without the express written permission of Fresh Consulting.
    Bellevue, Washington

    File Description:
       This is test code to test things in the sandbox.

    Used by:
        <P/N> <Description>

    Notes:
        None

    Modification History:
        1 2021-08-02 Initial revision.
-------------------------------------------------------------------------
"""

# ----------------------------------------------------------------------------
#  Includes
# ----------------------------------------------------------------------------
import datetime
import logging

# ----------------------------------------------------------------------------
#  Consts
# ----------------------------------------------------------------------------
FILE_NAME = "../../data/textfiles/WabashCannonball.txt"
GREEN_TEXT = "\033[1;32m"
NORMAL_TEXT = "\033[0m"


def main():
    """----------------------------------------------------------------------.

        Purpose:
            Starting point for the application

        Notes:
            None

    -----------------------------------------------------------------------
    """
    logging.info("running count words module!")
    file = open(FILE_NAME, "rt")
    data = file.read()
    words = data.split()

    print(GREEN_TEXT, "Number of words in text file :", len(words), NORMAL_TEXT)
    logging.info("Number of words in text file :" + str(len(words)))


# ----------------------------------------------------------------------------
#  Purpose:
#   Entry point for file
#
#  Notes:
#      This is also the entry point for the application
#
# ----------------------------------------------------------------------------
if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    logging.info(f"{datetime.datetime.now()}: starting application")
    main()
